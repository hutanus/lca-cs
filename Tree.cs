﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Lowest_Common_Ancestor {
    public class Tree {
        public string Name { get; set; }
        public Tree[] PointsTo { get; set; }

        public Tree() {
            Name = "";
            PointsTo = new Tree[0];
        }

        // Depth-first list of nodes in a tree
        public List<string> AllNodes() {
            string currentNodeName = Name ?? "";

            List<string> allNodes = new List<string>() { currentNodeName };
            foreach(Tree node in PointsTo) {
                // Only add unique nodes
                List<string> nodesToAdd = node.AllNodes();
                foreach(string newNode in nodesToAdd)
                    if(!allNodes.Contains(newNode))
                        allNodes.Add(newNode);
            }

            return allNodes;
        }
        public bool NodeExists(string nodeName) {
            // Successful case
            if (Name == nodeName)
                return true;

            // Case where this node is not the target
            bool isInChildren = false;
            foreach(Tree child in PointsTo)
                if(child.NodeExists(nodeName))
                    isInChildren = true;

            return isInChildren;
        }

        public void PathsToNodeRecursive(List<List<string>> allPaths, List<string> currentPath, string nodeName) {
            // Add this node to our current path
            currentPath.Add(Name);

            // Base case, we found one of the paths to this node
            if(Name == nodeName) {
                // Add this newly found path to our currently accumulating path
                // (using .ToList() here to create a new list rather than pass by reference)
                allPaths.Add(currentPath.ToList());
            } else {
                // Node is not here, look in the children
                foreach(Tree child in PointsTo)
                    // Find all the paths to our target node in this child
                    child.PathsToNodeRecursive(allPaths, currentPath, nodeName);
            }
            // We're done with this path, so remove this node from it and backtrack
            //   to find another path
            currentPath.RemoveAt(currentPath.Count - 1);
        }

        public List<List<string>> PathsToNode(string nodeName) {
            List<List<string>> result = new List<List<string>>();
            PathsToNodeRecursive(result, new List<string>(), nodeName);
            return result;
        }

        public string LowestCommonAncestor(string firstNodeName, string secondNodeName) {
            List<List<string>> pathsToFirstNode = PathsToNode(firstNodeName);
            List<List<string>> pathsToSecondNode = PathsToNode(secondNodeName);
            int ancestorOverhead = int.MaxValue;    // Used for finding the lowest common ancestor
            List<string> pathToLowestCommonAncestor = new List<string>();

            // The lowest common ancestor of the a node is the parent of that node
            if(firstNodeName == secondNodeName) {
                // Get the shortest path
                List<string> pathToNode = pathsToFirstNode.OrderBy(path => path.Count).First();
                // If the tail of this path isn't the root, then backtrack to its parent
                if(pathToNode.Count > 1)
                    pathToNode.RemoveAt(pathToNode.Count - 1);
                // Return the last item in the path
                return pathToNode.Last();
            }

            // Return null if we were asked to find the lowest common ancestor of
            //   a node that's not in the tree
            if (pathsToFirstNode.Count == 0 || pathsToSecondNode.Count == 0)
                return null;

            // Loop through the each of the paths to the first and second nodes
            foreach (List<string> pathToFirstNode in pathsToFirstNode) {
                foreach (List<string> pathToSecondNode in pathsToSecondNode) {
                    // Keep a record of the path to the lowest common ancestor for two of
                    //   these paths
                    List<string> currentPathToLowestCommonAncestor = new List<string>();

                    // Loop up until we reach the end of the first or second list, excluding the nodes we're looking for
                    for (int i = 0; i < Math.Min(pathToFirstNode.Count, pathToSecondNode.Count) - 1; i++) {
                        // As long as the paths follow the same nodes...
                        if (pathToFirstNode[i] == pathToSecondNode[i])
                            // Build up a path leading to the lowest common ancestor
                            currentPathToLowestCommonAncestor.Add(pathToFirstNode[i]);
                        else
                            // And stop when they diverge
                            break;
                    }

                    // Update our path to the total lowest common ancestor if the distance from
                    //   this lowest common ancestor to the first node plus the distance to the second
                    //   node
                    int localOverhead = pathToFirstNode.Count - currentPathToLowestCommonAncestor.Count;
                    localOverhead += pathToSecondNode.Count - currentPathToLowestCommonAncestor.Count;
                    if(localOverhead < ancestorOverhead) {
                        pathToLowestCommonAncestor = currentPathToLowestCommonAncestor.ToList();
                        ancestorOverhead = localOverhead;
                    }
                }
            }

            // If we haven't found a lowest common ancestor, return the root node
            if(pathToLowestCommonAncestor.Count == 0)
                return pathsToFirstNode[0][0];
            else
                return pathToLowestCommonAncestor.Last();
        }
    }
}
