# Lowest Common Ancestor (C#)


This is my repository for solving the lowest common ancestor for CSU33012. This version is written in C#, because I found LINQ (a powerful data structure query language) interesting. I also chose this language for the second part of the assignment (making it work with directed acyclic graphs) because the test explorer in Visual Studio was a great visual way of knowing which tests passed and which ones didn't.

## Running lca-cs
To run this application, you'll need to have Visual Studio installed (or another C# IDE). You can load the project using `Lowest Common Ancestor.sln`. You can then use the built in debugger on the "Lowest Common Ancestor" project to run the command line interface. To run the tests, go to "View -> Test explorer" and click the double green arrow.

## Tree files

I've included three tree files in the res folder.

* `full_depth_2.json` is a tree with root node "A" and two child nodes "B" and "C".
* `full_depth_3.json` is identical to `full_depth_2.json` but with another level below "B" and "C".
* `wikipedia-G.json` is a directed acyclic graph based on a graph from the Wikipedia page for Directed Acyclic Graphs [(to the right of this link)](https://en.wikipedia.org/wiki/Directed_acyclic_graph#Reachability,_transitive_closure,_and_transitive_reduction).

In addition to these two files, I've also included some other files (for testing purposes) under `test_files`. These files won't be shown in the command line interface.

* `left_and_right_undefined.json` is a tree with a root node "A" with child nodes "left" and "right" set to empty objects
* `no_left_or_right.json` is a tree with a root node "A" with neither "left" nor "right" defined.
* `wikipedia-G` is a transative version of the graph from the Wikipedia page.

## Algorithm

The lowest common ancestor is calculated by comparing the paths between two nodes from the root. For example, the lowest common ancestor between C and D is B if their paths are ABC and ABD

## Repository structure

* The `res` folder contains sample trees (in JSON format)
* The `tests` folder contains tests for the `Tree.cs` and `TreeManager.cs` files
* `Tree.cs` represents a tree and has methods for calculating the lowest common ancestor
* `TreeManager.cs` reads and parses the tree files in the `res` folder
* `TreeDescriptor.cs` has two classes that are used for parsing the files in `TreeManager.cs`.