﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lowest_Common_Ancestor {
    public class TreeDescriptor {
        public string Name { get; set; }
        public string[] PointsTo { get; set; }
    }

    public class TreeFileModel {
        public TreeDescriptor[] Nodes { get; set; }
    }
}
