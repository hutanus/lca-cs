﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Lowest_Common_Ancestor {
    public class TreeManager {
        public static List<Tree> GetAllTrees() {
            // Get all the filepaths to our trees
            List<string> allTreePaths = GetAllTreeFilenames();

            // Get a list of tree objects from those json files
            List<Tree> allTrees = allTreePaths.Select(treePath => GetTree(treePath)).ToList();

            return allTrees;
        }

        public static Tree GetTree(string PathToTree) {
            // Read the contents of the JSON file
            StreamReader jsonReader = new StreamReader(PathToTree);
            string json = jsonReader.ReadToEnd();

            // Parse the JSON file as a list of objects, where each object
            // is a tree node descriptor
            TreeDescriptor[] descriptors = JsonConvert.DeserializeObject<TreeFileModel>(json).Nodes;
            // Create a list to hold a flat list of all our trees
            Tree[] allTrees = new Tree[descriptors.Length];

            // Return an empty tree if there are no descriptors
            if (descriptors.Length == 0)
                return new Tree();

            // Make a tree out of every tree descriptor
            for(int i = 0; i < descriptors.Length; i++) {
                Tree newTree = new Tree();
                newTree.Name = descriptors[i].Name;
                allTrees[i] = newTree;
            }

            // Build up the tree hierarchy
            // For each tree...
            for(int treeIndex = 0; treeIndex < descriptors.Length; treeIndex++) {
                // If it points to any other nodes...
                if(descriptors[treeIndex].PointsTo != null) {
                    // Initialize its PointsTo array, ...
                    allTrees[treeIndex].PointsTo = new Tree[descriptors[treeIndex].PointsTo.Length];
                    // Loop through each other tree it points to...
                    for (int i = 0; i < descriptors[treeIndex].PointsTo.Length; i++) {
                        // And make this tree point to it:
                        allTrees[treeIndex].PointsTo[i] = allTrees.Where(tree => tree.Name == descriptors[treeIndex].PointsTo[i]).First();
                    }
                } else {
                    // If it doesn't point to any other nodes, initialize it with an empty array
                    allTrees[treeIndex].PointsTo = new Tree[0];
                }
            }

            // Return the root node (the tree from the first descriptor)
            return allTrees[0];
        }

        public static List<string> GetAllTreeFilenames() {
            // Get the full path to the folder containing our tree json files
            string projectPath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
            string resFolderPath = projectPath + "\\res";

            // Get a list of filepaths to our tree files
            DirectoryInfo resFolder = new DirectoryInfo(resFolderPath);
            List<string> allTreePaths = resFolder.GetFiles("*.json").Select(file => file.FullName).ToList();

            return allTreePaths;
        }
    }
}
