﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Lowest_Common_Ancestor {
    class Program
    {
        static void Main(string[] args)
        {
            // Read and parse all the trees in the "res" folder
            List<Tree> allTrees = TreeManager.GetAllTrees();
            Console.WriteLine($"{allTrees.Count} tree files found:\n");

            // Print a list of the trees and their filenames to the user for selection
            List<string> allTreeFilenames = TreeManager.GetAllTreeFilenames();
            for (int i = 0; i < allTreeFilenames.Count; i++)
                Console.WriteLine($"[{i}]: {allTreeFilenames[i].Split('\\').Last()}");

            // Ask the user to select one
            Console.Write("\nEnter the index of the tree to work with: ");
            string indexAsString = Console.ReadLine();
            int selectedTreeIndex = -1;

            // Make sure the user entered an integer
            if(!Int32.TryParse(indexAsString, out selectedTreeIndex)) {
                Console.WriteLine("Please enter a number");
                Console.ReadKey();
                return;
            }

            // Make sure the user selected a valid tree
            if(selectedTreeIndex < 0 || selectedTreeIndex >= allTrees.Count) {
                Console.WriteLine($"Please enter a number between 0 and {allTrees.Count - 1}");
                Console.ReadKey();
                return;
            }

            // Show the user what nodes are available for selection
            Tree selectedTree = allTrees[selectedTreeIndex];
            string selectedTreePath = allTreeFilenames[selectedTreeIndex].Split('\\').Last();
            Console.WriteLine($"Selected tree {selectedTreePath} with nodes:");
            selectedTree.AllNodes().ForEach(node => Console.Write($"{node} "));

            // Ask the user for the first node
            Console.Write("\nPlease select the first node: ");
            string firstNode = Console.ReadLine();
            if(!selectedTree.NodeExists(firstNode)) {
                Console.WriteLine($"Couldn't find the node {firstNode}");
                Console.ReadKey();
                return;
            }

            // Ask the user for the second node
            Console.Write("\nPlease select the first node: ");
            string secondNode = Console.ReadLine();
            if(!selectedTree.NodeExists(secondNode)) {
                Console.WriteLine($"Couldn't find the node {secondNode}");
                Console.ReadKey();
                return;
            }

            // Calculate and display the lowest common ancestor
            string lowestCommonAncestor = selectedTree.LowestCommonAncestor(firstNode, secondNode);
            Console.WriteLine($"The lowest common ancestor between {firstNode} and {secondNode} is {lowestCommonAncestor}");

            // Wait for the user to enter something before closing the window
            Console.ReadKey();
        }
    }
}
