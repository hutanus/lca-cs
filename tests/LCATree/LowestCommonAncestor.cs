﻿using Lowest_Common_Ancestor;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;

namespace Unit_Tests.LCATree {
    [TestClass]
    public class LowestCommonAncestor {
        [TestMethod]
        public void EmptyTree() {
            Tree testTree = new Tree();

            // "There are no paths to node "A""
            Assert.AreEqual(testTree.PathsToNode("A").Count, 0);
        }

        [TestMethod]
        public void NonExistentNode() {
            string projectPath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.Parent.FullName;
            Tree testTree = TreeManager.GetTree($"{projectPath}\\res\\full_depth_2.json");

            Assert.AreEqual(testTree.LowestCommonAncestor("X", "Y"), null);
        }

        [TestMethod]
        public void Depth2() {
            string projectPath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.Parent.FullName;
            Tree testTree = TreeManager.GetTree($"{projectPath}\\res\\full_depth_2.json");

            Assert.AreEqual(testTree.LowestCommonAncestor("A", "A"), "A");
            Assert.AreEqual(testTree.LowestCommonAncestor("A", "B"), "A");
            Assert.AreEqual(testTree.LowestCommonAncestor("A", "C"), "A");
            Assert.AreEqual(testTree.LowestCommonAncestor("B", "C"), "A");
            Assert.AreEqual(testTree.LowestCommonAncestor("B", "B"), "A");
            Assert.AreEqual(testTree.LowestCommonAncestor("C", "C"), "A");
        }

        [TestMethod]
        public void Depth3() {
            string projectPath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.Parent.FullName;
            Tree testTree = TreeManager.GetTree($"{projectPath}\\res\\full_depth_3.json");

            Assert.AreEqual(testTree.LowestCommonAncestor("A", "A"), "A");
            Assert.AreEqual(testTree.LowestCommonAncestor("A", "B"), "A");
            Assert.AreEqual(testTree.LowestCommonAncestor("A", "C"), "A");
            Assert.AreEqual(testTree.LowestCommonAncestor("B", "C"), "A");
            Assert.AreEqual(testTree.LowestCommonAncestor("B", "B"), "A");
            Assert.AreEqual(testTree.LowestCommonAncestor("C", "C"), "A");

            Assert.AreEqual(testTree.LowestCommonAncestor("D", "E"), "B");
            Assert.AreEqual(testTree.LowestCommonAncestor("F", "G"), "C");
        }

        [TestMethod]
        public void WikipediaG() {
            string projectPath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.Parent.FullName;
            Tree testTree = TreeManager.GetTree($"{projectPath}\\res\\wikipedia-G.json");

            Assert.AreEqual(testTree.LowestCommonAncestor("B", "D"), "A");
            Assert.AreEqual(testTree.LowestCommonAncestor("D", "E"), "C");
            Assert.AreEqual(testTree.LowestCommonAncestor("E", "E"), "A");
            Assert.AreEqual(testTree.LowestCommonAncestor("D", "C"), "A");
        }

        [TestMethod]
        public void WikipediaTransativeG() {
            string projectPath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.Parent.FullName;
            Tree testTree = TreeManager.GetTree($"{projectPath}\\res\\test_files\\wikipedia-transative-G.json");


            Assert.AreEqual(testTree.LowestCommonAncestor("D", "E"), "B");
            Assert.AreEqual(testTree.LowestCommonAncestor("B", "C"), "A");
            Assert.AreEqual(testTree.LowestCommonAncestor("E", "E"), "D");
            Assert.AreEqual(testTree.LowestCommonAncestor("D", "C"), "A");
        }
    }
}
