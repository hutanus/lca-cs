﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Lowest_Common_Ancestor;
using System.IO;

namespace Unit_Tests.LCATree {
    [TestClass]
    public class NodeExists {
        [TestMethod]
        public void EmptyTree() {
            Tree testTree = new Tree();

            Assert.IsFalse(testTree.NodeExists("A"));
        }

        [TestMethod]
        public void NonExistentNode() {
            string projectPath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.Parent.FullName;
            Tree testTree = TreeManager.GetTree($"{projectPath}\\res\\full_depth_2.json");

            Assert.IsFalse(testTree.NodeExists("X"));
        }

        [TestMethod]
        public void FullDepth3() {
            string projectPath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.Parent.FullName;
            Tree testTree = TreeManager.GetTree($"{projectPath}\\res\\full_depth_3.json");

            Assert.IsTrue(testTree.NodeExists("A"));
            Assert.IsTrue(testTree.NodeExists("B"));
            Assert.IsTrue(testTree.NodeExists("C"));
            Assert.IsTrue(testTree.NodeExists("D"));
            Assert.IsTrue(testTree.NodeExists("E"));
            Assert.IsTrue(testTree.NodeExists("F"));
            Assert.IsTrue(testTree.NodeExists("G"));
            Assert.IsFalse(testTree.NodeExists("H"));
        }

        [TestMethod]
        public void NoChildren() {
            string projectPath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.Parent.FullName;
            Tree testTree = TreeManager.GetTree($"{projectPath}\\res\\test_files\\no_children.json");

            Assert.IsTrue(testTree.NodeExists("A"));
            Assert.IsFalse(testTree.NodeExists("B"));
        }

        [TestMethod]
        public void WikipediaG() {
            string projectPath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.Parent.FullName;
            Tree testTree = TreeManager.GetTree($"{projectPath}\\res\\wikipedia-G.json");

            Assert.IsTrue(testTree.NodeExists("A"));
            Assert.IsTrue(testTree.NodeExists("B"));
            Assert.IsTrue(testTree.NodeExists("C"));
            Assert.IsTrue(testTree.NodeExists("D"));
            Assert.IsTrue(testTree.NodeExists("E"));
            Assert.IsFalse(testTree.NodeExists("X"));
        }

        [TestMethod]
        public void WikipediaTransativeG() {
            string projectPath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.Parent.FullName;
            Tree testTree = TreeManager.GetTree($"{projectPath}\\res\\test_files\\wikipedia-transative-G.json");

            Assert.IsTrue(testTree.NodeExists("A"));
            Assert.IsTrue(testTree.NodeExists("B"));
            Assert.IsTrue(testTree.NodeExists("C"));
            Assert.IsTrue(testTree.NodeExists("D"));
            Assert.IsTrue(testTree.NodeExists("E"));
            Assert.IsFalse(testTree.NodeExists("X"));
        }
    }
}
