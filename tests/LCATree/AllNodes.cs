using Microsoft.VisualStudio.TestTools.UnitTesting;
using Lowest_Common_Ancestor;
using System.IO;
using System.Collections.Generic;
using System.Linq;

namespace Unit_Tests.LCATree {
    [TestClass]
    public class AllNodes {
        [TestMethod]
        public void EmptyTree() {
            Tree testTree = new Tree();
            Assert.IsTrue(testTree.AllNodes().SequenceEqual(new List<string>() { "" }));
        }

        [TestMethod]
        public void FullDepth2() {
            string projectPath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.Parent.FullName;
            Tree testTree = TreeManager.GetTree($"{projectPath}\\res\\full_depth_2.json");

            List<string> fullDepth2List = new List<string> { "A", "B", "C" };
            Assert.IsTrue(testTree.AllNodes().SequenceEqual(fullDepth2List));
        }

        [TestMethod]
        public void FullDepth3() {
            string projectPath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.Parent.FullName;
            Tree testTree = TreeManager.GetTree($"{projectPath}\\res\\full_depth_3.json");

            List<string> fullDepth3List = new List<string> { "A", "B", "D", "E", "C", "F", "G" };
            Assert.IsTrue(testTree.AllNodes().SequenceEqual(fullDepth3List));
        }

        [TestMethod]
        public void NoChildren() {
            string projectPath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.Parent.FullName;
            Tree testTree = TreeManager.GetTree($"{projectPath}\\res\\test_files\\no_children.json");

            List<string> noChildrenList = new List<string> {"A"};
            Assert.IsTrue(testTree.AllNodes().SequenceEqual(noChildrenList));
        }

        [TestMethod]
        public void WikipediaG() {
            string projectPath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.Parent.FullName;
            Tree testTree = TreeManager.GetTree($"{projectPath}\\res\\wikipedia-G.json");

            List<string> wikipediaGList = new List<string> { "A", "B", "D", "E", "C" };
            Assert.IsTrue(testTree.AllNodes().SequenceEqual(wikipediaGList));
        }

        [TestMethod]
        public void WikipediaTransativeG() {
            string projectPath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.Parent.FullName;
            Tree testTree = TreeManager.GetTree($"{projectPath}\\res\\test_files\\wikipedia-transative-G.json");

            List<string> wikipediaTransativeGList = new List<string> { "A", "B", "D", "E", "C" };
            Assert.IsTrue(testTree.AllNodes().SequenceEqual(wikipediaTransativeGList));
        }
    }
}
