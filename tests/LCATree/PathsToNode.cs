﻿using Lowest_Common_Ancestor;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Unit_Tests.LCATree {
    [TestClass]
    public class PathsToNode {
        [TestMethod]
        public void EmptyTree() {
            Tree testTree = new Tree();

            // "There are no paths to node "A""
            Assert.AreEqual(testTree.PathsToNode("A").Count, 0);
        }

        [TestMethod]
        public void NonExistentNode() {
            string projectPath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.Parent.FullName;
            Tree testTree = TreeManager.GetTree($"{projectPath}\\res\\full_depth_3.json");

            Assert.AreEqual(testTree.PathsToNode("X").Count, 0);
        }

        [TestMethod]
        public void Depth2() {
            string projectPath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.Parent.FullName;
            Tree testTree = TreeManager.GetTree($"{projectPath}\\res\\full_depth_2.json");

            List<string> pathToA = new List<string>() { "A" };
            // There is one path to node A
            Assert.AreEqual(testTree.PathsToNode("A").Count, 1);
            Assert.IsTrue(testTree.PathsToNode("A")[0].SequenceEqual(pathToA));

            List<string> pathToB = new List<string>() { "A", "B" };
            // There is one path to node B
            Assert.AreEqual(testTree.PathsToNode("B").Count, 1);
            Assert.IsTrue(testTree.PathsToNode("B")[0].SequenceEqual(pathToB));

            List<string> pathToC = new List<string>() { "A", "C" };
            // There is one path to node C
            Assert.AreEqual(testTree.PathsToNode("C").Count, 1);
            Assert.IsTrue(testTree.PathsToNode("C")[0].SequenceEqual(pathToC));
        }

        [TestMethod]
        public void Depth3() {
            string projectPath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.Parent.FullName;
            Tree testTree = TreeManager.GetTree($"{projectPath}\\res\\full_depth_3.json");

            List<string> pathToA = new List<string>() { "A" };
            // There is one path to node A
            Assert.AreEqual(testTree.PathsToNode("A").Count, 1);
            Assert.IsTrue(testTree.PathsToNode("A")[0].SequenceEqual(pathToA));

            List<string> pathToB = new List<string>() { "A", "B" };
            // There is one path to node B
            Assert.AreEqual(testTree.PathsToNode("B").Count, 1);
            Assert.IsTrue(testTree.PathsToNode("B")[0].SequenceEqual(pathToB));

            List<string> pathToC = new List<string>() { "A", "C" };
            // There is one path to node C
            Assert.AreEqual(testTree.PathsToNode("C").Count, 1);
            Assert.IsTrue(testTree.PathsToNode("C")[0].SequenceEqual(pathToC));

            List<string> pathToD = new List<string>() { "A", "B", "D" };
            // There is one path to node D
            Assert.AreEqual(testTree.PathsToNode("D").Count, 1);
            Assert.IsTrue(testTree.PathsToNode("D")[0].SequenceEqual(pathToD));

            List<string> pathToE = new List<string>() { "A", "B", "E" };
            // There is one path to node E
            Assert.AreEqual(testTree.PathsToNode("E").Count, 1);
            Assert.IsTrue(testTree.PathsToNode("E")[0].SequenceEqual(pathToE));

            List<string> pathToF = new List<string>() { "A", "C", "F" };
            // There is one path to node F
            Assert.AreEqual(testTree.PathsToNode("F").Count, 1);
            Assert.IsTrue(testTree.PathsToNode("F")[0].SequenceEqual(pathToF));

            List<string> pathToG = new List<string>() { "A", "C", "G" };
            // There is one path to node G
            Assert.AreEqual(testTree.PathsToNode("G").Count, 1);
            Assert.IsTrue(testTree.PathsToNode("G")[0].SequenceEqual(pathToG));
        }

        [TestMethod]
        public void WikipediaG() {
            string projectPath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.Parent.FullName;
            Tree testTree = TreeManager.GetTree($"{projectPath}\\res\\wikipedia-G.json");

            List<string> pathToA = new List<string>() { "A" };
            // There is one path to node A
            Assert.AreEqual(testTree.PathsToNode("A").Count, 1);
            Assert.IsTrue(testTree.PathsToNode("A")[0].SequenceEqual(pathToA));

            List<string> pathToB = new List<string>() { "A", "B" };
            // There is one path to node B
            Assert.AreEqual(testTree.PathsToNode("B").Count, 1);
            Assert.IsTrue(testTree.PathsToNode("B")[0].SequenceEqual(pathToB));

            List<string> pathToC = new List<string>() { "A", "C" };
            // There is one path to node C
            Assert.AreEqual(testTree.PathsToNode("C").Count, 1);
            Assert.IsTrue(testTree.PathsToNode("C")[0].SequenceEqual(pathToC));

            // There are 3 paths to D
            Assert.AreEqual(testTree.PathsToNode("D").Count, 3);
            List<string> pathToD = new List<string>() { "A", "B", "D" };
            Assert.IsTrue(testTree.PathsToNode("D")[0].SequenceEqual(pathToD));
            pathToD = new List<string>() { "A", "C", "D" };
            Assert.IsTrue(testTree.PathsToNode("D")[1].SequenceEqual(pathToD));
            pathToD = new List<string>() { "A", "D" };
            Assert.IsTrue(testTree.PathsToNode("D")[2].SequenceEqual(pathToD));

            // There are 5 paths to E
            Assert.AreEqual(testTree.PathsToNode("E").Count, 5);
            List<string> pathToE = new List<string>() { "A", "B", "D", "E" };
            Assert.IsTrue(testTree.PathsToNode("E")[0].SequenceEqual(pathToE));
            pathToE = new List<string>() { "A", "C", "D", "E" };
            Assert.IsTrue(testTree.PathsToNode("E")[1].SequenceEqual(pathToE));
            pathToE = new List<string>() { "A", "C", "E" };
            Assert.IsTrue(testTree.PathsToNode("E")[2].SequenceEqual(pathToE));
            pathToE = new List<string>() { "A", "D", "E" };
            Assert.IsTrue(testTree.PathsToNode("E")[3].SequenceEqual(pathToE));
            pathToE = new List<string>() { "A", "E" };
            Assert.IsTrue(testTree.PathsToNode("E")[4].SequenceEqual(pathToE));
        }

        [TestMethod]
        public void WikipediaTransativeG() {
            string projectPath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.Parent.FullName;
            Tree testTree = TreeManager.GetTree($"{projectPath}\\res\\test_files\\wikipedia-transative-G.json");

            List<string> pathToA = new List<string>() { "A" };
            // There is one path to node A
            Assert.AreEqual(testTree.PathsToNode("A").Count, 1);
            Assert.IsTrue(testTree.PathsToNode("A")[0].SequenceEqual(pathToA));

            List<string> pathToB = new List<string>() { "A", "B" };
            // There is one path to node B
            Assert.AreEqual(testTree.PathsToNode("B").Count, 1);
            Assert.IsTrue(testTree.PathsToNode("B")[0].SequenceEqual(pathToB));

            List<string> pathToC = new List<string>() { "A", "C" };
            // There is one path to node C
            Assert.AreEqual(testTree.PathsToNode("C").Count, 1);
            Assert.IsTrue(testTree.PathsToNode("C")[0].SequenceEqual(pathToC));

            // There are 2 paths to D
            Assert.AreEqual(testTree.PathsToNode("D").Count, 2);
            List<string> pathToD = new List<string>() { "A", "B", "D" };
            Assert.IsTrue(testTree.PathsToNode("D")[0].SequenceEqual(pathToD));
            pathToD = new List<string>() { "A", "C", "D" };
            Assert.IsTrue(testTree.PathsToNode("D")[1].SequenceEqual(pathToD));

            // There are 2 paths to E
            Assert.AreEqual(testTree.PathsToNode("E").Count, 2);
            List<string> pathToE = new List<string>() { "A", "B", "D", "E" };
            Assert.IsTrue(testTree.PathsToNode("E")[0].SequenceEqual(pathToE));
            pathToE = new List<string>() { "A", "C", "D", "E" };
            Assert.IsTrue(testTree.PathsToNode("E")[1].SequenceEqual(pathToE));
        }
    }
}