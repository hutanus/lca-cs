﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Lowest_Common_Ancestor;
using System.IO;

namespace Unit_Tests.LCATreeManager {
    [TestClass]
    public class GetTree {
        [TestMethod]
        public void Depth3() {
            string projectPath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.Parent.FullName;
            Tree testTree = TreeManager.GetTree($"{projectPath}\\res\\full_depth_3.json");

            Assert.AreEqual(testTree.Name, "A");

            Assert.AreEqual(testTree.PointsTo[0].Name, "B");
            Assert.AreEqual(testTree.PointsTo[1].Name, "C");

            Assert.AreEqual(testTree.PointsTo[0].PointsTo[0].Name, "D");
            Assert.AreEqual(testTree.PointsTo[0].PointsTo[1].Name, "E");
            Assert.AreEqual(testTree.PointsTo[1].PointsTo[0].Name, "F");
            Assert.AreEqual(testTree.PointsTo[1].PointsTo[1].Name, "G");
        }

        [TestMethod]
        public void NoChildren() {
            string projectPath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.Parent.FullName;
            Tree testTree = TreeManager.GetTree($"{projectPath}\\res\\test_files\\no_children.json");
            Assert.AreEqual(testTree.Name, "A");
            Assert.IsTrue(testTree.PointsTo.Length == 0);
        }
    }
}
